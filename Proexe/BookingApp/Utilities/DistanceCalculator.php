<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {

	    //Niestety ale tutaj sam nie napisałęm tego algorytmu, nie ma co wykonywać koła na nowo jak algorytm został już napisany to pozostała transkrypcja na php
        //Implementacja SQL:
        // select (2* atan(sqrt(sin(radians(lat-[podajlat])/2) * sin(radians(lat-[podajlat])/2) + cos(radians([podajlat])) * cos(radians(lat)) * sin(radians(lng-[podajLon])/2) * sin(radians(lng-[podajLon])/2)) ,sqrt(1-sin(radians(lat-[podajlat])/2) * sin(radians(lat-[podajlat])/2) + cos(radians([podajlat])) * cos(radians(lat)) * sin(radians(lng-[podajLon])/2) * sin(radians(lng-[podajLon])/2)))) * [podajPromienWMiarze]
        // from offices;
        // dużo łatwiej było by zrobić to w PL/SQL, procedurze lub funkcji mysql niż zapytaniu.



	    $R = 6371;
        if($unit=='m')
        $R*=1000;
        $dLat = deg2rad($to[0]-$from[0]);
        $dLon = deg2rad($to[1]-$from[1]);

        $a =sin($dLat/2) * sin($dLat/2) +
            cos(deg2rad($from[0])) * cos(deg2rad($to[0])) *
            sin($dLon/2) * sin($dLon/2);

        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $d = $R * $c; // Distance in km
        return $d;


	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
        $minDist=null;
        foreach ($offices as $office) {
            $dist = $this->calculate($from,[ $office['lat'], $office['lng'] ],'km' );

        if ($dist < (is_null($minDist) ? $minDist=$dist: $minDist)){
            $minDist = $dist;
            $closestOffice = $office;
        }
        }
		return $closestOffice['name'];
	}

}
